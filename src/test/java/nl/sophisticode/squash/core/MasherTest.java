package nl.sophisticode.squash.core;

import io.cucumber.datatable.DataTable;
import nl.sophisticode.squash.core.helpers.Animals;
import nl.sophisticode.squash.core.helpers.TestDataBuilder;
import nl.sophisticode.squash.core.objects.Dog;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertThrows;

class MasherTest {
    private static final Animals.AsMap animals = Animals.asMap;
    private static final Animals.AsObject animalObjects = Animals.asObject;

    @Test
    void isEqual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.isEqual(expected, actual);
    }

    @Test
    void isEqualWithMapping() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.withDateTime().buster())
                .row(animals.withDateTime().spike())
                .row(animals.withDateTime().bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.isEqual(expected, actual);
    }

    @Test
    void isEqualWithDataTable() {
        final List<List<String>> expected = Arrays.asList(
                headers(),
                Arrays.asList("Buster", "5", "2007-08-09", "Canidae"),
                Arrays.asList("Spike", "7", "2008-03-23", "Canidae"),
                Arrays.asList("Bailey", "12", "2005-06-15", "Canidae")
        );
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();

        Masher.isEqual(DataTable.create(expected), actual);
    }

    @Test
    void isEqualWithDataTableAndMapping() {
        final List<List<String>> expected = Arrays.asList(
                headers(),
                Arrays.asList("Buster", "5", "2007-08-09T00:00", "Canidae"),
                Arrays.asList("Spike", "7", "2008-03-23T00:00", "Canidae"),
                Arrays.asList("Bailey", "12", "2005-06-15T00:00", "Canidae")
        );
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.isEqual(DataTable.create(expected), actual);
    }

    @Test
    void isEqualFailsWithUnorderedDataTable() {
        final List<List<String>> expected = Arrays.asList(
                headers(),
                new ArrayList<>(animals.spike().values()),
                new ArrayList<>(animals.buster().values()),
                new ArrayList<>(animals.bailey().values())
        );
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable isEqual = () -> Masher.isEqual(DataTable.create(expected), actual);
        assertThrows(AssertionError.class, isEqual, "Expected complaints about wrong order of Bailey and Spike");
    }

    @Test
    void isEqualFailsWithUnorderedDataTableAndMapping() {
        final List<List<String>> expected = Arrays.asList(
                headers(),
                new ArrayList<>(animals.spike().values()),
                new ArrayList<>(animals.buster().values()),
                new ArrayList<>(animals.bailey().values())
        );
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqual = () -> Masher.withMapping.isEqual(DataTable.create(expected), actual);
        assertThrows(AssertionError.class, isEqual, "Expected complaints about wrong order of Bailey and Spike");
    }

    @Test
    void isEqualFailsWithMissingObject() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(AssertionError.class, isEqual, "Expected missing Bailey");
    }

    @Test
    void isEqualFailsWithMissingObjectAndMapping() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike());
        final Executable isEqual = () -> Masher.withMapping.isEqual(expected, actual);
        assertThrows(AssertionError.class, isEqual, "Expected missing Bailey");
    }

    @Test
    void isEqualFailsWithMissingFieldInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row()
                    .putAll("name", "Bailey", "age", "12", "lastCheckup", "2005-06-15")
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(IllegalArgumentException.class, isEqual, "Expected missing 'family' field for Bailey");
    }

    @Test
    void isEqualFailsWithMissingFieldInExpected() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row()
                    .putAll("name", "Bailey", "age", "12", "lastCheckup", "2005-06-15")
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(IllegalArgumentException.class, isEqual, "Expected missing 'family' field for Bailey");
    }

    @Test
    void isEqualFailsWithMappingAndMissingFieldInExpected() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row()
                    .putAll("name", "Bailey", "age", "12", "lastCheckup", "2005-06-15")
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqual = () -> Masher.withMapping.isEqual(expected, actual);
        assertThrows(IllegalArgumentException.class, isEqual, "Expected missing 'family' field for Bailey");
    }

    @Test
    void isEqualFailsWithSuperfluousFieldInExpected() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row()
                    .putAll("name", "Bailey", "age", "12", "lastCheckup", "2005-06-15", "family", "Canidae", "goodBoy", "true")
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(IllegalArgumentException.class, isEqual, "Expected superfluous 'goodBoy' field for Bailey");
    }

    @Test
    void isEqualFailsWithMappingAndSuperfluousFieldInExpected() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row()
                    .putAll("name", "Bailey", "age", "12", "lastCheckup", "2005-06-15", "family", "Canidae", "goodBoy", "true")
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqual = () -> Masher.withMapping.isEqual(expected, actual);
        assertThrows(IllegalArgumentException.class, isEqual, "Expected superfluous 'goodBoy' field for Bailey");
    }

    @Test
    void isEqualFailsWithSuperfluousFieldInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row()
                    .putAll("name", "Bailey", "age", "12", "lastCheckup", "2005-06-15", "family", "Canidae", "goodBoy", "true")
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(IllegalArgumentException.class, isEqual, "Expected superfluous 'goodBoy' field for Bailey");
    }

    @Test
    void isEqualFailsWithSuperfluousObjectInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(AssertionError.class, isEqual, "Expected superfluous Spike");
    }

    @Test
    void isEqualFailsWithMappingAndSuperfluousObjectInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqual = () -> Masher.withMapping.isEqual(expected, actual);
        assertThrows(AssertionError.class, isEqual, "Expected superfluous Spike");
    }

    @Test
    void isEqualFailsWithWrongOrder() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.bailey())
                .row(animals.spike())
                .build();
        final Executable isEqual = () -> Masher.isEqual(expected, actual);
        assertThrows(AssertionError.class, isEqual, "Expected complaints about wrong order of Bailey and Spike");
    }

    @Test
    void isEqualFailsWithMappingAndWrongOrder() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqual = () -> Masher.withMapping.isEqual(expected, actual);
        assertThrows(AssertionError.class, isEqual, "Expected complaints about wrong order of Bailey and Spike");
    }

    @Test
    void isEqualUnordered() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.spike())
                .row(animals.bailey())
                .row(animals.buster())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.isEqualUnordered(expected, actual);
    }

    @Test
    void isEqualUnorderedWithMapping() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.withDateTime().spike())
                .row(animals.withDateTime().bailey())
                .row(animals.withDateTime().buster())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.isEqualUnordered(expected, actual);
    }

    @Test
    void isEqualUnorderedWithDataTable() {
        final List<List<String>> expected = Arrays.asList(
                headers(),
                Arrays.asList("Buster", "5", "2007-08-09", "Canidae"),
                Arrays.asList("Spike", "7", "2008-03-23", "Canidae"),
                Arrays.asList("Bailey", "12", "2005-06-15", "Canidae")
        );
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.isEqualUnordered(DataTable.create(expected), actual);
    }

    @Test
    void isEqualUnorderedWithMappingAndDataTable() {
        final List<List<String>> expected = Arrays.asList(
                headers(),
                Arrays.asList("Buster", "5", "2007-08-09T00:00", "Canidae"),
                Arrays.asList("Spike", "7", "2008-03-23T00:00", "Canidae"),
                Arrays.asList("Bailey", "12", "2005-06-15T00:00", "Canidae")
        );
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.isEqualUnordered(DataTable.create(expected), actual);
    }

    @Test
    void isEqualUnorderedFailsWithDataTableWithMissingItem() {
        final List<List<String>> expected = Arrays.asList(
                new ArrayList<>(animals.spike().values()),
                new ArrayList<>(animals.bailey().values())
        );
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable isEqualUnordered = () -> Masher.isEqualUnordered(DataTable.create(expected), actual);
        assertThrows(AssertionError.class, isEqualUnordered, "Expected missing Buster");
    }

    @Test
    void isEqualUnorderedFailsWithMappingAndDataTableWithMissingItem() {
        final List<List<String>> expected = Arrays.asList(
                new ArrayList<>(animals.spike().values()),
                new ArrayList<>(animals.bailey().values())
        );
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqualUnordered = () -> Masher.withMapping.isEqualUnordered(DataTable.create(expected), actual);
        assertThrows(AssertionError.class, isEqualUnordered, "Expected missing Buster");
    }

    @Test
    void isEqualUnorderedWithSameOrder() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.isEqualUnordered(expected, actual);
    }

    @Test
    void isEqualUnorderedWithMappingAndSameOrder() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.withDateTime().buster())
                .row(animals.withDateTime().spike())
                .row(animals.withDateTime().bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.isEqualUnordered(expected, actual);
    }

    @Test
    void isEqualUnorderedFailsWithMissingObjectInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .build();
        final Executable isEqualUnordered = () -> Masher.isEqualUnordered(expected, actual);
        assertThrows(AssertionError.class, isEqualUnordered, "Expected missing Bailey");
    }

    @Test
    void isEqualUnorderedFailsWithMappingAndMissingObjectInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike());
        final Executable isEqualUnordered = () -> Masher.withMapping.isEqualUnordered(expected, actual);
        assertThrows(AssertionError.class, isEqualUnordered, "Expected missing Bailey");
    }

    @Test
    void isEqualUnorderedFailsWithSuperfluousObjectInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.bailey())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable isEqualUnordered = () -> Masher.isEqualUnordered(expected, actual);
        assertThrows(AssertionError.class, isEqualUnordered, "Expected superfluous Spike");
    }

    @Test
    void isEqualUnorderedFailsWithMappingAndSuperfluousObjectInActual() {
        final List<Map<String, String>> expected = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.bailey())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable isEqualUnordered = () -> Masher.withMapping.isEqualUnordered(expected, actual);
        assertThrows(AssertionError.class, isEqualUnordered, "Expected superfluous Spike");
    }

    @Test
    void contains() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.contains(shouldContain, actual);
    }

    @Test
    void containsWithMapping() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.withDateTime().buster())
                .row(animals.withDateTime().spike())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.contains(shouldContain, actual);
    }

    @Test
    void containsWithDataTable() {
        final List<List<String>> shouldContain = Arrays.asList(
                headers(),
                Arrays.asList("Buster", "5", "2007-08-09", "Canidae"),
                Arrays.asList("Spike", "7", "2008-03-23", "Canidae")
        );
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.contains(DataTable.create(shouldContain), actual);
    }

    @Test
    void containsWithMappingAndDataTable() {
        final List<List<String>> shouldContain = Arrays.asList(
                headers(),
                Arrays.asList("Buster", "5", "2007-08-09T00:00", "Canidae"),
                Arrays.asList("Spike", "7", "2008-03-23T00:00", "Canidae")
        );
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.contains(DataTable.create(shouldContain), actual);
    }

    @Test
    void containsUnordered() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.spike())
                .row(animals.buster())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        Masher.contains(shouldContain, actual);
    }

    @Test
    void containsUnorderedWithMapping() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.withDateTime().spike())
                .row(animals.withDateTime().buster())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        Masher.withMapping.contains(shouldContain, actual);
    }

    @Test
    void containsFailsWithMissingObjectInActualButSameSize() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable contains = () -> Masher.contains(shouldContain, actual);
        assertThrows(AssertionError.class, contains, "Expected missing Buster");
    }

    @Test
    void containsFailsWithMappingAndMissingObjectInActualButSameSize() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.spike(), animalObjects.bailey());
        final Executable contains = () -> Masher.withMapping.contains(shouldContain, actual);
        assertThrows(AssertionError.class, contains, "Expected missing Buster");
    }

    @Test
    void containsFailsWithMissingFieldInActual() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.spike())
                .row(animals.buster())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row()
                    .putAll("name", "Buster", "lastCheckup", "2007-08-09", "family", "Canidae")
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable contains = () -> Masher.contains(shouldContain, actual);
        assertThrows(IllegalArgumentException.class, contains, "Expected missing 'age' field for Buster");
    }

    @Test
    void containsFailsWithSuperfluousFieldInExpected() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row()
                    .putAll("name", "Buster", "lastCheckup", "2007-08-09", "family", "Canidae", "goodBoy", "true")
                .row(animals.spike())
                .row(animals.buster())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable contains = () -> Masher.contains(shouldContain, actual);
        assertThrows(AssertionError.class, contains, "Expected superfluous 'goodBoy' field for Buster");
    }

    @Test
    void containsFailsWithMappingAndSuperfluousFieldInExpected() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row()
                    .putAll("name", "Buster", "lastCheckup", "2007-08-09", "family", "Canidae", "goodBoy", "true")
                .row(animals.spike())
                .row(animals.buster())
                .build();
        final List<Dog> actual = Arrays.asList(animalObjects.buster(), animalObjects.spike(), animalObjects.bailey());
        final Executable contains = () -> Masher.withMapping.contains(shouldContain, actual);
        assertThrows(AssertionError.class, contains, "Expected superfluous 'goodBoy' field for Buster");
    }

    @Test
    void containsFailsWithSuperfluousFieldInActual() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.buster())
                .row(animals.spike())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row()
                    .putAll("name", "Buster", "lastCheckup", "2007-08-09", "family", "Canidae", "goodBoy", "true")
                .row(animals.spike())
                .row(animals.bailey())
                .build();
        final Executable contains = () -> Masher.contains(shouldContain, actual);
        assertThrows(AssertionError.class, contains, "Expected superfluous 'goodBoy' field for Buster");
    }

    @Test
    void containsSucceedsWithSuperfluousFieldInActualWithUnmatchedRow() {
        final List<Map<String, String>> shouldContain = new TestDataBuilder()
                .row(animals.bailey())
                .row(animals.spike())
                .build();
        final List<Map<String, String>> actual = new TestDataBuilder()
                .row(animals.spike())
                .row(animals.bailey())
                .row()
                    .putAll("name", "Buster", "lastCheckup", "2007-08-09", "family", "Canidae", "goodBoy", "true")
                .build();
        Masher.contains(shouldContain, actual);
    }

    private List<String> headers() {
        return Arrays.asList("name", "age", "lastCheckup", "family");
    }

}