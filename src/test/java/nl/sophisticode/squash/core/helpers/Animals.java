package nl.sophisticode.squash.core.helpers;

import nl.sophisticode.squash.core.objects.Dog;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.Map;

public class Animals {
    public static final AsObject asObject = new AsObject();
    public static final AsMap asMap = new AsMap();
    private static final AsMap.WithDateTime withDateTime = new AsMap.WithDateTime();

    public static class AsObject {
        public Dog buster() {
            return new Dog(
                    "Buster",
                    5,
                    LocalDateTime.of(2007, Month.AUGUST, 9, 0, 0));
        }

        public Dog spike() {
            return new Dog(
                    "Spike",
                    7,
                    LocalDateTime.of(2008, Month.MARCH, 23, 0, 0));
        }

        public Dog bailey() {
            return new Dog(
                    "Bailey",
                    12,
                    LocalDateTime.of(2005, Month.JUNE, 15, 0, 0));
        }
    }

    public static class AsMap {
        public WithDateTime withDateTime() {
            return withDateTime;
        }

        public Map<String, String> buster() {
            return new HashMap<String, String>() {{
                put("name", "Buster");
                put("age", "5");
                put("lastCheckup", "2007-08-09");
                put("family", "Canidae");
            }};
        }

        public Map<String, String> spike() {
            return new HashMap<String, String>() {{
                put("name", "Spike");
                put("age", "7");
                put("lastCheckup", "2008-03-23");
                put("family", "Canidae");
            }};
        }

        public Map<String, String> bailey() {
            return new HashMap<String, String>() {{
                put("name", "Bailey");
                put("age", "12");
                put("lastCheckup", "2005-06-15");
                put("family", "Canidae");
            }};
        }

        public static class WithDateTime extends AsMap {
            @Override
            public Map<String, String> buster() {
                return addTime(super.buster());
            }

            @Override
            public Map<String, String> spike() {
                return addTime(super.spike());
            }

            @Override
            public Map<String, String> bailey() {
                return addTime(super.bailey());
            }

            private Map<String, String> addTime(final Map<String, String> input) {
                input.compute("lastCheckup", (k, v) -> v.concat("T00:00"));
                return input;
            }
        }
    }
}
