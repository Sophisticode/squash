package nl.sophisticode.squash.core.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TestDataBuilder {
    private final List<Map<String, String>> result;

    public TestDataBuilder() {
        result = new ArrayList<>();
    }

    public RowBuilder row() {
        return new RowBuilder(this);
    }

    public TestDataBuilder row(final Map<String, String> row) {
        result.add(row);
        return this;
    }

    public List<Map<String, String>> build() {
        return result;
    }

    public class RowBuilder {
        private final TestDataBuilder parent;
        private final MapBuilder row;

        RowBuilder(final TestDataBuilder parent) {
            this.parent = parent;
            this.row = new MapBuilder();
        }

        public TestDataBuilder putAll(final String... items) {
            row.putAll(items);
            parent.result.add(row.build());
            return parent;
        }
    }
}
