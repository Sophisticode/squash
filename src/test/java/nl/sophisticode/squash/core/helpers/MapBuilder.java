package nl.sophisticode.squash.core.helpers;

import java.util.HashMap;
import java.util.Map;

public class MapBuilder {
    private static final String ODD_NUMBER_OF_ARGUMENTS_MESSAGE = "The expected arguments are key, value, key, value, etc." +
            " But the received number of arguments was %d, which means that a key or value is missing somewhere.";
    private Map<String, String> result;

    MapBuilder() {
        result = new HashMap<>();
    }

    public MapBuilder putAll(final String... items) {
        if (items.length % 2 > 0)
            throw new IllegalArgumentException(String.format(ODD_NUMBER_OF_ARGUMENTS_MESSAGE, items.length));

        for (int i = 0; i < items.length; i += 2) {
            result.put(items[i], items[i + 1]);
        }
        return this;
    }

    public Map<String, String> build() {
        return result;
    }
}
