package nl.sophisticode.squash.core;

import nl.sophisticode.squash.core.helpers.Animals;
import nl.sophisticode.squash.core.objects.Dog;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class SquasherTest {
    private static final Animals.AsObject animals = Animals.asObject;

    @Test
    void squashDefault() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
            put("family", "Canidae");
        }};
        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashWithDateConversion() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09");
            put("family", "Canidae");
        }};
        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .conversion("lastCheckup", (LocalDateTime original) -> String.valueOf(original.toLocalDate()))
                .build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashWithMultipleConversions() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Busters");
            put("age", "6");
            put("lastCheckup", "2007-08-09");
            put("family", "Canidae");
        }};
        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .conversion("name", (String original) -> original + "s")
                .conversion("age", (Integer original) -> String.valueOf(original + 1))
                .conversion("lastCheckup", (LocalDateTime original) -> String.valueOf(original.toLocalDate()))
                .build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashIncludingAllButOneField() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
        }};
        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .includeFields(new ArrayList<>(expectedDog.keySet()))
                .build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashIncludingCertainFieldsUsingMatchFields() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
        }};
        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .matchFields(expectedDog)
                .build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashExcludingSuperclassFields() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
        }};
        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .excludeSuperclassFields()
                .build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashInferringType() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
            put("family", "Canidae");
        }};
        final Squasher sq = Squasher.builder().build();

        final Map<String, String> actualDog = sq.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashUsingOnceMethodWithImplicitFields() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
            put("family", "Canidae");
        }};

        final Map<String, String> actualDog = Squasher.once.squash(originalDog);
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashUsingOnceMethodWithExplicitFields() {
        final Dog originalDog = animals.buster();
        final Map<String, String> expectedDog = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
            put("family", "Canidae");
        }};

        final Map<String, String> actualDog = Squasher.once.squash(originalDog, new ArrayList<>(expectedDog.keySet()));
        checkEquality(expectedDog, actualDog);
    }

    @Test
    void squashWithMultipleObjects() {
        final List<Dog> originalDogs = Arrays.asList(animals.buster(), animals.spike(), animals.bailey());
        final Map<String, String> expectedBuster = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
            put("family", "Canidae");
        }};
        final Map<String, String> expectedSpike = new HashMap<String, String>() {{
            put("name", "Spike");
            put("age", "7");
            put("lastCheckup", "2008-03-23T00:00");
            put("family", "Canidae");
        }};
        final Map<String, String> expectedBailey = new HashMap<String, String>() {{
            put("name", "Bailey");
            put("age", "12");
            put("lastCheckup", "2005-06-15T00:00");
            put("family", "Canidae");
        }};
        final List<Map<String, String>> expectedDogs = Arrays.asList(expectedBuster, expectedSpike, expectedBailey);

        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .build();
        final List<Map<String, String>> actualDogs = sq.squash(originalDogs);
        checkEquality(expectedDogs, actualDogs);
    }

    @Test
    void squashWithMultipleObjectsExcludingFields() {
        final List<Dog> originalDogs = Arrays.asList(animals.buster(), animals.spike(), animals.bailey());
        final Map<String, String> expectedBuster = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
        }};
        final Map<String, String> expectedSpike = new HashMap<String, String>() {{
            put("name", "Spike");
            put("age", "7");
        }};
        final Map<String, String> expectedBailey = new HashMap<String, String>() {{
            put("name", "Bailey");
            put("age", "12");
        }};
        final List<Map<String, String>> expectedDogs = Arrays.asList(expectedBuster, expectedSpike, expectedBailey);

        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .build();
        final List<Map<String, String>> actualDogs = sq.squash(originalDogs, Arrays.asList("name", "age"));
        checkEquality(expectedDogs, actualDogs);
    }

    @Test
    void squashWithMultipleObjectsExcludingFieldsAndConversion() {
        final List<Dog> originalDogs = Arrays.asList(animals.buster(), animals.spike(), animals.bailey());
        final Map<String, String> expectedBuster = new HashMap<String, String>() {{
            put("name", "Busters");
            put("age", "5");
        }};
        final Map<String, String> expectedSpike = new HashMap<String, String>() {{
            put("name", "Spikes");
            put("age", "7");
        }};
        final Map<String, String> expectedBailey = new HashMap<String, String>() {{
            put("name", "Baileys");
            put("age", "12");
        }};
        final List<Map<String, String>> expectedDogs = Arrays.asList(expectedBuster, expectedSpike, expectedBailey);

        final Squasher sq = Squasher.builder()
                .type(Dog.class)
                .conversion("name", (String original) -> original + "s")
                .build();
        final List<Map<String, String>> actualDogs = sq.squash(originalDogs, Arrays.asList("name", "age"));
        checkEquality(expectedDogs, actualDogs);
    }

    @Test
    void squashWithMultipleObjectsUsingOnce() {
        final List<Dog> originalDogs = Arrays.asList(animals.buster(), animals.spike(), animals.bailey());
        final Map<String, String> expectedBuster = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
            put("lastCheckup", "2007-08-09T00:00");
            put("family", "Canidae");
        }};
        final Map<String, String> expectedSpike = new HashMap<String, String>() {{
            put("name", "Spike");
            put("age", "7");
            put("lastCheckup", "2008-03-23T00:00");
            put("family", "Canidae");
        }};
        final Map<String, String> expectedBailey = new HashMap<String, String>() {{
            put("name", "Bailey");
            put("age", "12");
            put("lastCheckup", "2005-06-15T00:00");
            put("family", "Canidae");
        }};
        final List<Map<String, String>> expectedDogs = Arrays.asList(expectedBuster, expectedSpike, expectedBailey);

        final List<Map<String, String>> actualDogs = Squasher.once.squash(originalDogs);
        checkEquality(expectedDogs, actualDogs);
    }

    @Test
    void squashWithMultipleObjectsUsingOnceExcludingFields() {
        final List<Dog> originalDogs = Arrays.asList(animals.buster(), animals.spike(), animals.bailey());
        final Map<String, String> expectedBuster = new HashMap<String, String>() {{
            put("name", "Buster");
            put("age", "5");
        }};
        final Map<String, String> expectedSpike = new HashMap<String, String>() {{
            put("name", "Spike");
            put("age", "7");
        }};
        final Map<String, String> expectedBailey = new HashMap<String, String>() {{
            put("name", "Bailey");
            put("age", "12");
        }};
        final List<Map<String, String>> expectedDogs = Arrays.asList(expectedBuster, expectedSpike, expectedBailey);

        final List<Map<String, String>> actualDogs = Squasher.once.squash(originalDogs, Arrays.asList("name", "age"));
        checkEquality(expectedDogs, actualDogs);
    }

    private void checkEquality(final Map expected, final Map actual) {
        assertArrayEquals(expected.entrySet().toArray(), actual.entrySet().toArray());
    }

    private void checkEquality(final List<Map<String, String>> expected, final List<Map<String, String>> actual) {
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

}
