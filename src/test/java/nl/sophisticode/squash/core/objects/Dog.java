package nl.sophisticode.squash.core.objects;

import java.time.LocalDateTime;

public class Dog extends Animal {
    private final String name;
    private final int age;
    private final LocalDateTime lastCheckup;

    public Dog(String name, int age, LocalDateTime lastCheckup) {
        super("Canidae");

        this.name = name;
        this.age = age;
        this.lastCheckup = lastCheckup;
    }
}
