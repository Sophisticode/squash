package nl.sophisticode.squash.core;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ConfiguredSquasher extends Squasher {
    private final SquasherConfig config;

    ConfiguredSquasher(final SquasherConfig config) {
        this.config = config;
    }

    @Override
    public List<Map<String, String>> squash(final List<?> subjects) {
        return squash(subjects, config.getIncludedFields());
    }

    @Override
    public List<Map<String, String>> squash(final List<?> subjects, final List<String> fieldsToInclude) {
        final Predicate<Field> filter = getFieldsFilter(fieldsToInclude);
        return subjects.stream()
                .map(subject -> super.squash(subject, config, filter))
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, String> squash(final Object subject) {
        return squash(subject, config.getIncludedFields());
    }

    @Override
    public Map<String, String> squash(final Object subject, final List<String> fieldsToInclude) {
        return super.squash(subject, config, getFieldsFilter(fieldsToInclude));
    }
}
