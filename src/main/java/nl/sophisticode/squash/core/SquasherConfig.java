package nl.sophisticode.squash.core;

import nl.sophisticode.squash.core.converters.Converter;
import nl.sophisticode.squash.core.converters.DefaultConverter;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class SquasherConfig {
    private final Map<String, Converter> conversions;
    private final List<Field> declaredFields;
    private final List<String> includedFields;
    private final boolean includeSuperclassFields;

    SquasherConfig(final Class<?> type, final Map<String, Converter> conversions, final List<String> includedFields,
                   final boolean includeSuperclassFields) {
        this.conversions = conversions;
        this.includedFields = includedFields;
        this.includeSuperclassFields = includeSuperclassFields;

        declaredFields = collectFields(type);
    }

    public List<Field> getDeclaredFields(final Object subject) {
        if (declaredFields.isEmpty()) {
            return collectFields(subject.getClass());
        }

        return declaredFields;
    }

    private List<Field> collectFields(final Class<?> type) {
        if (type == null) {
            return Collections.emptyList();
        }

        final List<Field> result = Arrays.stream(type.getDeclaredFields())
                .collect(Collectors.toList());

        if (includeSuperclassFields) {
            collectSuperclassFields(type, result);
        }

        return result;
    }

    private List<Field> collectSuperclassFields(final Class<?> type, final List<Field> fields) {
        final Class<?> superclass = type.getSuperclass();
        if (superclass == null) {
            return fields;
        }

        fields.addAll(Arrays.asList(superclass.getDeclaredFields()));
        return collectSuperclassFields(superclass, fields);
    }

    private Map<String, Converter> getConversions() {
        return conversions;
    }

    public Converter getConverter(final Field field) {
        return getConversions().getOrDefault(field.getName(), DefaultConverter.instance);
    }

    public List<String> getIncludedFields() {
        return includedFields;
    }
}
