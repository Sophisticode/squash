package nl.sophisticode.squash.core;

import nl.sophisticode.squash.core.converters.Converter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class SquasherBuilder {
    private Class<?> type;
    private final Map<String, Converter> conversions;
    private final List<String> includedFields;
    private boolean includeSuperclassFields;

    SquasherBuilder() {
        conversions = new HashMap<>();
        includedFields = new ArrayList<>();
        includeSuperclassFields = true;
    }

    public SquasherBuilder type(final Class<?> type) {
        this.type = type;
        return this;
    }

    public <T> SquasherBuilder conversion(final String field, final Converter<T> conversion) {
        conversions.put(field, conversion);
        return this;
    }

    public SquasherBuilder excludeSuperclassFields() {
        includeSuperclassFields = false;
        return this;
    }

    public SquasherBuilder includeFields(final List<String> fields) {
        includedFields.addAll(fields);
        return this;
    }

    public SquasherBuilder matchFields(final Map<?, ?> keysToMatch) {
        includeFields(keysToMatch.keySet().stream()
                .map(String::valueOf)
                .collect(Collectors.toList()));
        return this;
    }

    public SquasherConfig buildConfig() {
        return new SquasherConfig(type, conversions, includedFields, includeSuperclassFields);
    }

    public Squasher build() {
        return new ConfiguredSquasher(buildConfig());
    }
}
