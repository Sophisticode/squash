package nl.sophisticode.squash.core;

import nl.sophisticode.squash.core.converters.Converter;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Squasher {
    public static final Squasher once = new Squasher();

    Squasher() {}

    public List<Map<String, String>> squash(final List<?> subjects) {
        return squash(subjects, Collections.emptyList());
    }

    public List<Map<String, String>> squash(final List<?> subjects, final List<String> fieldsToInclude) {
        if (subjects == null || subjects.isEmpty()) {
            return new ArrayList<>();
        }

        final Predicate<Field> filter = getFieldsFilter(fieldsToInclude);
        final SquasherConfig config = builder()
                .type(subjects.get(0).getClass())
                .buildConfig();
        return subjects.stream()
                .map(subject -> squash(subject, config, filter))
                .collect(Collectors.toList());
    }

    public Map<String, String> squash(final Object subject) {
        final SquasherConfig config = builder()
                .type(subject.getClass())
                .buildConfig();
        return squash(subject, config, getFieldsFilter(Collections.emptyList()));
    }

    public Map<String, String> squash(final Object subject, final List<String> fieldsToInclude) {
        final SquasherConfig config = builder()
                .type(subject.getClass())
                .buildConfig();
        return squash(subject, config, getFieldsFilter(fieldsToInclude));
    }

    public Map<String, String> squash(final Object subject, final SquasherConfig config,
                                      final Predicate<Field> fieldsToInclude) {
        return config.getDeclaredFields(subject).stream()
                .filter(fieldsToInclude)
                .map(field -> {
                    final Object raw = tryGetValue(field, subject);
                    final Converter converter = config.getConverter(field);
                    final String value = converter.apply(raw);
                    return new MappedField(field.getName(), value);
                })
                .collect(Collectors.toMap(MappedField::getField, MappedField::getValue));
    }

    protected static Object tryGetValue(final Field field, final Object subject) {
        try {
            field.setAccessible(true);
            return field.get(subject);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return null;
    }

    protected static Predicate<Field> getFieldsFilter(final List<String> fieldsToInclude) {
        return (fieldsToInclude != null && !fieldsToInclude.isEmpty())
                ? (Field field) -> fieldsToInclude.contains(field.getName())
                : (Field field) -> true;
    }

    public static SquasherBuilder builder() {
        return new SquasherBuilder();
    }

}
