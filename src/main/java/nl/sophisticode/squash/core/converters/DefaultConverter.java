package nl.sophisticode.squash.core.converters;

public class DefaultConverter implements Converter<Object> {
    public static final DefaultConverter instance = new DefaultConverter();

    @Override
    public String apply(Object original) {
        return String.valueOf(original);
    }
}
