package nl.sophisticode.squash.core.converters;

import java.util.function.Function;

public interface Converter<T> extends Function<T, String> {
    @Override
    String apply(T original);
}
