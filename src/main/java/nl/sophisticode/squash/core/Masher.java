package nl.sophisticode.squash.core;

import io.cucumber.datatable.DataTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import static io.cucumber.datatable.matchers.DataTableHasTheSameRowsAs.hasTheSameRowsAs;
import static org.hamcrest.MatcherAssert.assertThat;

public class Masher {
    public static final MappingMasher withMapping = new MappingMasher();

    public static void isEqual(final DataTable expected, final List<Map<String, String>> actual) {
        isEqual(expected.asMaps(), actual);
    }

    public static void isEqual(final List<Map<String, String>> expected, final List<Map<String, String>> actual) {
        assertThat(toDataTable(actual), hasTheSameRowsAs(toDataTable(expected)).inOrder());
    }

    public static void isEqualUnordered(final DataTable expected, final List<Map<String, String>> actual) {
        isEqualUnordered(expected.asMaps(), actual);
    }

    public static void isEqualUnordered(final List<Map<String, String>> expected, final List<Map<String, String>> actual) {
        assertThat(toDataTable(actual), hasTheSameRowsAs(toDataTable(expected)));
    }

    public static void contains(final DataTable shouldContain, final List<Map<String, String>> actual) {
        contains(shouldContain.asMaps(), actual);
    }

    public static void contains(List<Map<String, String>> shouldContain, final List<Map<String, String>> actual) {
        final List<Map<String, String>> found = new ArrayList<>(actual);
        found.retainAll(shouldContain);
        final DataTable expected = toDataTable(shouldContain);
        final DataTable foundDt = toDataTable(found);

        if (hasTheSameRowsAs(expected).matches(foundDt))
            return;

        assertThat("Here is the list that was expected, where the missing rows are marked", toDataTable(actual), hasTheSameRowsAs(expected));
    }

    static class MappingMasher {
        public void isEqual(final DataTable expected, final List<?> actual) {
            Masher.isEqual(expected, Squasher.once.squash(actual));
        }

        public void isEqual(final List<Map<String, String>> expected, final List<?> actual) {
            Masher.isEqual(expected, Squasher.once.squash(actual));
        }

        public void isEqualUnordered(final DataTable expected, final List<?> actual) {
            Masher.isEqualUnordered(expected, Squasher.once.squash(actual));
        }

        public void isEqualUnordered(final List<Map<String, String>> expected, final List<?> actual) {
            Masher.isEqualUnordered(expected, Squasher.once.squash(actual));
        }

        public void contains(final List<Map<String, String>> shouldContain, final List<?> actual) {
            Masher.contains(shouldContain, Squasher.once.squash(actual));
        }

        public void contains(final DataTable shouldContain, final List<?> actual) {
            Masher.contains(shouldContain, Squasher.once.squash(actual));
        }
    }

    private static DataTable toDataTable(List<Map<String, String>> input) {
        input = getSortedByKey(input);
        if (input == null || input.isEmpty())
            return DataTable.emptyDataTable();

        return DataTable.create(toListOfStrings(input));
    }

    private static List<List<String>> toListOfStrings(final List<Map<String, String>> input) {
        final List<List<String>> result = new ArrayList<>();
        input.stream()
                .map(Map::values)
                .map(ArrayList::new)
                .forEach(result::add);
        result.add(0, getHeaders(input));
        return result;
    }

    private static List<String> getHeaders(final List<Map<String, String>> input) {
        if (input == null || input.isEmpty())
            return null;

        return new ArrayList<>(input.get(0).keySet());
    }

    private static List<Map<String, String>> getSortedByKey(final List<Map<String, String>> unsorted) {
        return unsorted.stream()
                .map(TreeMap::new)
                .collect(Collectors.toList());
    }
}
