package nl.sophisticode.squash.core;

class MappedField {
    private final String field;
    private final String value;

    MappedField(final String field, final String value) {
        this.field = field;
        this.value = value;
    }

    String getField() {
        return field;
    }

    String getValue() {
        return value;
    }
}
